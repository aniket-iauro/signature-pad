import { Component } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})

export class AppComponent  {
  points = [];
  signatureImage;
  sanitizer;

  // I initialize the app component.
	constructor( sanitizer: DomSanitizer ) {

		this.sanitizer = sanitizer;

	}

  showImage(data) {
    console.log(data);
    this.signatureImage = data;
  }
  // This function is used to convert base64 encoding to mime type (blob)
base64ToBlob(base64, mime) 
{
    mime = mime || '';
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];

    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, {type: mime});
}



  saveImage(data){
    this.signatureImage = data.replace("png","jpeg"); //URL.createObjectURL(blob);
    console.log(this.signatureImage);
    // var jpegFile64 = this.signatureImage.replace(/^data:image\/(png|jpeg);base64,/, "");
    // var jpegBlob = this.base64ToBlob(jpegFile64, 'image/jpeg');  
    // console.log(jpegBlob);
    // this.signatureImage = URL.createObjectURL(jpegBlob);
    // URL.revokeObjectURL(this.signatureImage);
    // this.signatureImage = this.sanitizer.bypassSecurityTrustUrl( this.signatureImage )

    console.log(""+this.signatureImage);
  }
}
